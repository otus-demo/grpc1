﻿using System.Threading.Tasks;
using ConsoleClient1;
using Grpc.Net.Client;
using ConsoleClient1;

Thread.Sleep(5000);
// The port number must match the port of the gRPC server.
using var channel = GrpcChannel.ForAddress("https://localhost:6012");
var client = new Greeter.GreeterClient(channel);
var reply = await client.SayHelloAsync(
                  new HelloRequest { Name = "GreeterClient" });
Console.WriteLine("Greeting: " + reply.Message);
Console.WriteLine("Press any key to exit...");
Console.ReadKey();
